package com.tianyalei.core.zuulauth.exception;


/**
 * @author wuweifeng wrote on 2017/10/27.
 */
public class IpRefuseException extends Exception {

    public int code;

    public IpRefuseException() {
        super("IP不在白名单，或IP在黑名单");
    }

    public IpRefuseException(Throwable throwable, String sMessage, int nStatusCode) {
        super("IP不在白名单，或IP在黑名单", throwable, false, false);
    }

    public IpRefuseException(int code, String msg) {
        super(msg);
        this.code = code;
    }

    @Override
    public String toString() {
        return "IpRefuseException{" +
                "code=" + code +
                '}';
    }

}
