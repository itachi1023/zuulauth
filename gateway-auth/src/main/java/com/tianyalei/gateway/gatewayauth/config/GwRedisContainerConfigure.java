package com.tianyalei.gateway.gatewayauth.config;


import com.tianyalei.core.zuulauth.config.RedisContainerConfigure;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.cloud.gateway.config.GatewayAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;


/**
 * @author liyunfeng 2020-1-14
 */

@Configuration
@ConditionalOnMissingBean(RedisMessageListenerContainer.class)
@ConditionalOnClass(GatewayAutoConfiguration.class) //这一句是当前工程是gateway工程时，才启用该configuration
public class GwRedisContainerConfigure extends RedisContainerConfigure {

}

